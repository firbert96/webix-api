require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const app = express()
const mongoose = require('mongoose')

const dbConnectionString = {
    development: process.env.DB_CONNECTION,
    test: process.env.DB_CONNECTION,
    staging: process.env.DB_CONNECTION,
    production: process.env.DB_CONNECTION_PRODUCTION
}

mongoose.connect(dbConnectionString[process.env.NODE_ENV || 'development'],
    {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }
)
.then(() => {
    console.log(`Database is Connected`)
})
.catch((err) => {
    console.log(err)
    process.exit(1)
})

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())
app.use(morgan('tiny'))

app.get('/',(req,res)=>{
    res.status(200).json({
        success:true,
        data:"Welcome to webix-app"
    })
})
const taskRouter = require('./routers/taskRouter')
app.use('/api/v1',taskRouter)
module.exports = app
