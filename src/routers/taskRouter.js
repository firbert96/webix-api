const router = require('express').Router()

const task = require('../controllers/taskController')
router.post('/tasks',task.createTask)
router.get('/tasks/one',task.getATask)
router.get('/tasks/all',task.getAllTask)
router.put('/tasks',task.updateTask)
router.delete('/tasks',task.deleteTask)
module.exports = router