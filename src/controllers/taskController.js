const Task = require('../models/taskModel')
const {success,error} = require('../helpers/response')

exports.createTask = (req,res) =>{
    Task.create(req.body)
    .then(data=>{
        success(res,data,201)
    })
    .catch(err=>{
        error(res,err,422)
    })
}

exports.getATask = (req,res) =>{
    Task.findById(req.query.id)
    .then(data=>{
        success(res,data,200)
    })
    .catch(err=>{
        error(res,err,422)
    })
}

exports.getAllTask = (req,res) =>{
    Task.find()
    .then(data=>{
        success(res,data,200)
    })
    .catch(err=>{
        error(res,err,422)
    })
}

exports.updateTask = (req,res) =>{
    if(!req.body.id) return error(res,'Id cannot null',404)
    const id = {"_id":req.body.id}
    let updatedData = req.body
    delete updatedData.id
    Task.findByIdAndUpdate(id,updatedData,{ runValidators: true })
    .then(()=>{
        success(res,'Successfully updated data',200)
    })
    .catch(err=>{
        error(res,err,422)
    })
}
 
exports.deleteTask = (req,res) =>{
    if(!req.body.id) return error(res,'Id cannot null',404)
    Task.findByIdAndDelete(req.body.id)
    .then(()=>{
        success(res,'Successfully deleted data',200)
    })
    .catch(err=>{
        error(res,err,422)
    })
}